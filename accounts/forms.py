from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, label="Username")
    password = forms.CharField(
        max_length=150, label="Password", widget=forms.PasswordInput
    )


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150, label="Username")
    password = forms.CharField(
        max_length=150, widget=forms.PasswordInput, label="Password"
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
        label="Password Confirmation",
    )
