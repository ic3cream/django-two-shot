# Generated by Django 4.2 on 2023-04-19 16:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="receipt",
            name="tax",
            field=models.DecimalField(
                decimal_places=3, max_digits=10, null=True
            ),
        ),
        migrations.AlterField(
            model_name="receipt",
            name="account",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.account",
            ),
        ),
        migrations.AlterField(
            model_name="receipt",
            name="total",
            field=models.DecimalField(
                decimal_places=3, max_digits=10, null=True
            ),
        ),
    ]
